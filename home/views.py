from django.shortcuts import render
from django.http import Http404

def index(request):
    return render(request, 'home/index.html')
    
def index_ru(request):
    return render(request, 'home/index_ru.html')
    
    